## **Documentação da Disciplina de Administração de Sistemas Abertos (ASA)**

## **Resumo**

Essa documentação abordará os principais conteúdos da disciplina de Administração de Sistemas Abertos (ASA) do curso de Redes de Computadores, abandonando os principais conteúdos da primeira etapa do terceiro período, como configuração de interfaces de rede, instalação de servidor, como o servidor LAMP, servidor SSH, configuração de SSH, etc.

