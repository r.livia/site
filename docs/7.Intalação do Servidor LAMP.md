## **Configuração do Servidor LAMP**

O pacote LAMP é composto por quatro softwares de código aberto como o **Linux, o Apache2, MySQL e PHP**, juntos são úteis para a construção de um site. Ao longo desse documento aprenderemos como utilizá-los para a construção de um site.

### **Instalação do Apache2**

Instalação do Apache2 e seus utilitários 

```
sudo apt install apache2 apache2-utils -y
```

##### **Habilitando Serviço Apache 2**

Habilitando serviço Apache para iniciar na inicialização do sistema

```
sudo systemctl –now enable
```

#### **Iniciando serviço Apache** 

```
sudo systemctl start apache2 ou service status apache2
```

Verificação do Status do Apache2

### **Instalação do MariaDB**

##### **Sintaxe para a instalação do MariaDB**

```
apt install mariadb-server -y
```

##### **Reinicialização do Sistema**

```
systemctl enable mariadb

systemctl start mariadb
```

##### **Verificação do status da MariaDB**

````
systemctl status mariadb ou service status mariadb
````

### **Proteção do banco de dados do MariaDB**

Para todas as perguntas que forem feitas pelo sistema o usuário tem que responder sim.

````
mysql _secure_installation
````

### **Instalação do PHP**

##### Sintaxe de instalação
```
apt install php -cli php-mysql librapache2-mod-php php-gd php-xml php-curlphp-common -y
```
